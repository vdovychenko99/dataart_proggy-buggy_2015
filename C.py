n=int(input())
if (n>=100):
    print("UNDECIDABLE")
    exit()
elif (n<3):
    print("UNDECIDABLE")
    exit()
else:
    for i in range(2,n):
        if (n%i) == 0:
            print("UNDECIDABLE")
            exit()
    for i in range(n):
        for j in range(n):
            print(i, end=" ")
        print("\n")