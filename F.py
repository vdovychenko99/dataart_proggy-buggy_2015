n, m = map(int, input().split())
if n<1 or m>10000:
    print("UNDECIDABLE")
    exit()
nn=n
mm=m
if m<n:
    while mm!=nn:
        mm=mm+m
        if mm>nn:
            nn=nn+n
elif m==n:
    print(m)
else:
    while nn!=mm:
        nn=nn+n
        if nn>mm:
            mm=mm+m
print(nn)